import {useBook} from "../context/BookContext";
import Item from "../components/Item";

export default function PageList() {
    const { list, onSelect } = useBook();
    console.log(list);
    return (
        <section className={'list skip-banner'}>
            <ul>
                {list.map((item, i) => <li key={i}><Item {...item } onSelect={onSelect} /></li>)}
            </ul>
        </section>
    )
}
