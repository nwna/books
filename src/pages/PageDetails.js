import {useNavigate, useParams} from "react-router-dom";
import {useBook} from "../context/BookContext";
import {useEffect} from "react";
import {isDefined} from "../tools/helpers";

const NoBookFound = () => {
    const navigate = useNavigate();
    return (
        <>
        <h1>no book found </h1>
            <button onClick={() => navigate('/books')}>navigate back</button>
        </>
    );
}


export default function PageDetails() {
    const { fetchBookById, book } = useBook();
    const { id } = useParams();

    useEffect(
        () => {
            if (isDefined(id))
                fetchBookById(id);
        }, [fetchBookById, id]
    );

    if (!isDefined(id))
        return null;
    if (!isDefined(book))
        return <NoBookFound />
    return (
        <section className={'details'}>
            <h1>{id}</h1>
        </section>
    )
}