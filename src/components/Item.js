import {useCallback} from "react";

/**
 authors: ['George R. R. Martin']
 characters: (55) ['https://anapioficeandfire.com/api/characters/39', 'https://anapioficeandfire.com/api/characters/40', 'https://anapioficeandfire.com/api/characters/49', 'https://anapioficeandfire.com/api/characters/55', 'https://anapioficeandfire.com/api/characters/76', 'https://anapioficeandfire.com/api/characters/105', 'https://anapioficeandfire.com/api/characters/154', 'https://anapioficeandfire.com/api/characters/156', 'https://anapioficeandfire.com/api/characters/157', 'https://anapioficeandfire.com/api/characters/169', 'https://anapioficeandfire.com/api/characters/253', 'https://anapioficeandfire.com/api/characters/265', 'https://anapioficeandfire.com/api/characters/269', 'https://anapioficeandfire.com/api/characters/275', 'https://anapioficeandfire.com/api/characters/386', 'https://anapioficeandfire.com/api/characters/479', 'https://anapioficeandfire.com/api/characters/481', 'https://anapioficeandfire.com/api/characters/488', 'https://anapioficeandfire.com/api/characters/526', 'https://anapioficeandfire.com/api/characters/538', 'https://anapioficeandfire.com/api/characters/548', 'https://anapioficeandfire.com/api/characters/567', 'https://anapioficeandfire.com/api/characters/569', 'https://anapioficeandfire.com/api/characters/610', 'https://anapioficeandfire.com/api/characters/611', 'https://anapioficeandfire.com/api/characters/618', 'https://anapioficeandfire.com/api/characters/675', 'https://anapioficeandfire.com/api/characters/684', 'https://anapioficeandfire.com/api/characters/694', 'https://anapioficeandfire.com/api/characters/696', 'https://anapioficeandfire.com/api/characters/808', 'https://anapioficeandfire.com/api/characters/871', 'https://anapioficeandfire.com/api/characters/873', 'https://anapioficeandfire.com/api/characters/874', 'https://anapioficeandfire.com/api/characters/875', 'https://anapioficeandfire.com/api/characters/878', 'https://anapioficeandfire.com/api/characters/945', 'https://anapioficeandfire.com/api/characters/951', 'https://anapioficeandfire.com/api/characters/1050', 'https://anapioficeandfire.com/api/characters/1066', 'https://anapioficeandfire.com/api/characters/1076', 'https://anapioficeandfire.com/api/characters/1077', 'https://anapioficeandfire.com/api/characters/1291', 'https://anapioficeandfire.com/api/characters/1419', 'https://anapioficeandfire.com/api/characters/1458', 'https://anapioficeandfire.com/api/characters/1527', 'https://anapioficeandfire.com/api/characters/1528', 'https://anapioficeandfire.com/api/characters/1699', 'https://anapioficeandfire.com/api/characters/1739', 'https://anapioficeandfire.com/api/characters/1746', 'https://anapioficeandfire.com/api/characters/1833', 'https://anapioficeandfire.com/api/characters/1850', 'https://anapioficeandfire.com/api/characters/1884', 'https://anapioficeandfire.com/api/characters/1912', 'https://anapioficeandfire.com/api/characters/2138']
 country: "United States"
 isbn: "978-0345537263"
 mediaType: "Hardcover"
 name: "The Rogue Prince"
 numberOfPages: 832
 povCharacters: []
 publisher: "Bantam Books"
 released: "2014-06-17T00:00:00"
 url: "https://anapioficeandfire.com/api/books/10"
 */

export default function Item({ onSelect, ...props }) {

    const handlerSelect = useCallback(
        () => {
            onSelect(props);
        }, [onSelect, props]
    );

    return (
        <article className={'item'} onClick={handlerSelect}>
            <Header {...props} />
            <Body {...props} />
            <Footer {...props} />
        </article>
    )
}

const Header = ({ name, numberOfPages, authors }) => {
    return (
        <header className={'item-header'}>
            <h1>{name}</h1>
            <div className={'item-header-pages'}>
                <img src={'https://cdn3.iconfinder.com/data/icons/solid-locations-icon-set/64/LIBRARY_2-48.png'} />
                <p>{numberOfPages}</p>
            </div>
        </header>
    )
}

const Body = ({}) => {
    return (
        <div className={'item-body'}>
            <h1>{}</h1>
            <p>{}</p>
        </div>
    )
}

const Footer = () => {
    return (
        <div className={'item-footer'}>

        </div>
    )
}
