import React from "react";

export default function Header() {
    return (
        <header className={'banner'}>
            <h1>Books</h1>
        </header>
    )
}