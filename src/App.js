import './styles/App.scss';
import {BrowserRouter, Navigate, Route, Routes} from "react-router-dom";
import {BookContextProvider} from "./context/BookContext";
import {API_URL} from "./tools/constant";
import Header from "./components/Header";
import PageList from "./pages/PageList";
import PageDetails from "./pages/PageDetails";

function App() {
  return (
      <>
        <Header />
        <div className={'skip-banner'}>
            <BrowserRouter>
                <BookContextProvider apiUrl={API_URL}>
                    <Routes>
                        <Route path="/" element={<Navigate replace to="/books" />} />
                        <Route path={'/books'} element={<PageList />} />
                        <Route path={'/books/details/:id'} element={<PageDetails />} />
                        <Route path={'*'} element={<Navigate replace to={"/"} />} />
                    </Routes>
                </BookContextProvider>
            </BrowserRouter>
        </div>
      </>
  );
}

export default App;
