import React, {useCallback, useContext, useEffect, useMemo, useState} from "react";
import axios from "axios";
import {useNavigate} from "react-router-dom";

const BookContext = React.createContext({});
export const useBook = () => useContext(BookContext);

const extractBookUrl = book => book.url;
const extractBookId = book => {
    const url = extractBookUrl(book);
    return url.split('/').pop();
}

export const BookContextProvider = ({ apiUrl, children }) => {
    const [list, setList] = useState([]);
    const [book, setBook] = useState(null);

    const navigate = useNavigate();

    const flushList = useCallback(() => setList([]), []);

    const makeUrlWithBookId = useCallback(
        id => `${apiUrl}/${id}`, [apiUrl]
    );

    const fetchBookById = useCallback(
        (id) => {
            axios.get(makeUrlWithBookId(id)).then(r => r.data).then(setBook);
        }, [makeUrlWithBookId]
    );

    const fetchAllBooks = useCallback(() => {
        return axios.get(apiUrl).then(r => r.data).then(setList);
    }, [apiUrl]);

    const initList = useCallback(
        () => {
            fetchAllBooks();
        }, [fetchAllBooks]
    );

    const onSelect = useCallback(
        (book) => {
            const id = extractBookId(book);
            navigate(`/books/details/${id}`)
        }, [navigate]
    );

    useEffect(() => {
        initList();
        return flushList;
    }, [initList, flushList]);

    const dataContext = useMemo(
        () => {
            return {
                onSelect,
                list,
                fetchBookById,
                book,
            };
        }, [book, fetchBookById, list, onSelect]
    );

    return (
        <BookContext.Provider value={dataContext}>{children}</BookContext.Provider>
    )
}
